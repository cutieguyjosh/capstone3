//Create a JS file that will contain the logic for authentication and authorization via tokens and create a function to create an access token

const jwt = require('jsonwebtoken');
const secret = 'ThisIsASecret'


//Create JWT

module.exports.createAccessToken = (user) => {
	//Specify the information to be encoded in the JWT payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}



//Sign the JWT that specifies data sa payload (payload, secretkey, options)
	return jwt.sign(data, secret, {})	
}


//Verify authenticity of JWT
//Next, it passes on the request to the next middleware function/route/request handler in the stack

module.exports.verify = (req, res, next) => {
	//Get the token
	let token = req.headers.authorization
	if(typeof token !== 'undefined') {

	token = token.slice(7, token.length)
	//Once we have the token substring, we can now verify it using the verify() method of the jsonwebtoken package
	return jwt.verify(token, secret, (err, data) => {
		return (err) ? res.send({ auth: 'failed' }) : next()
	})

	//There was no token in the request
	} else {
		return res.send({ auth: 'failed' })
	}
}


//Decode JWT to get information from it (some user info is encoded in the payload)

module.exports.decode = (token) => {
	//The token parameter is for passing in the actual token calue as an argument

	if(typeof token !== 'undefined') {
		
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			//if verification results is error, do NOT decode
			return (err) ? null : jwt.decode(token, { complete: true} ).payload
			//{ complete: true } grabs both the request header and the payload 
		})
	} else {
		return null
	}
}