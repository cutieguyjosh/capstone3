const Transaction = require ('../models/Transaction')
const bcrypt = require('bcrypt')
const auth = require('../auth')


// Add a transaction

module.exports.addTransaction = async (params) => {
	const newTransaction = new Transaction({
		description: params.body.description,
		amount: params.body.amount,
		transactionType: params.body.transactionType,
		transactionCategory: params.body.transactionCategory,
		userId: params.userId
	});

	const result = await newTransaction.save();
	if (result) return { success: 'Successfully added' };
	else return { error: 'Something went wrong' };
};



// View all transactions

module.exports.records = async () => {
	return await Transaction.find({ status: 'Done' });
}



// View income transactions

module.exports.recordIncome = async () => {
	return await Transaction.find({ transactionType: 'Income' });
}



// View expense transactions

module.exports.recordExpense = async () => {
	return await Transaction.find({ transactionType: 'Expense' });
}
