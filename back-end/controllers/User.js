const User = require ('../models/User');
const bcrypt = require('bcrypt');
const { OAuth2Client } = require('google-auth-library');
const nodemailer = require('nodemailer');
const auth = require('../auth');
const clientId = '1090663674297-k8l4gku842tdu3048jptct64f47ej7o2.apps.googleusercontent.com';

//Check for duplicate email in the DB

module.exports.emailExists = (params) => {
	return User.find({ email:params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}


//Register a user

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNumber: params.mobileNumber,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'email'
	})

	return user.save().then((user, error) => {
		return(error) ? false : true
	})
}


//Login a user

module.exports.login = (params) => {
	return User.findOne({ email: params.email })
	.then(user => {

		if(user === null) {
			return { error: 'does-not-exist' }
		}

		if(user.loginType !== 'email') {
			return { error: 'login-type-error' }
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched) {
			return {
				accessToken: auth.createAccessToken(user.toObject())
			}
		} else {
			return { error: 'incorrect-passowrd' }
		}
	})
}


// Getting all the details of the user except for the password

module.exports.getDetails = (params) => {
	return User.findById(params.userId)
	.then((user, err) => {
		if(err) return false
			user.password = undefined
		return user
	})
}


// Verify the Google Token ID

module.exports.verifyGoogleTokenId = async (tokenId) => {

	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	});

	if (data.payload.email_verified === true) {

		const user = await User.findOne({ email: data.payload.email });

		if(user !== null) {
			if(user.loginType === 'google') {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			} else {
				return { error: 'login-type-error' };
			}
		} else {
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google',
			});

			return newUser.save().then((user,err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			});
		}
	} else {
		return { error: 'google-auth-error' };
	}
}