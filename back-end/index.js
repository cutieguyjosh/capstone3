const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config()

const app = express()
const port = process.env.PORT

app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect(process.env.DB_MONGODB, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

const userRoutes = require('./routes/User')
const transactionRoutes = require('./routes/Transaction')


app.use('/api/users', userRoutes)
app.use('/api/transaction', transactionRoutes)



app.listen(port, () => console.log(`You got served on port ${port}!`))