const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
	description: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true
	},
	transactionType: {
		type: String,
		required: true
	},
	transactionCategory: {
		type: String,
	},
	createdOn: {
		type: Date,
		default: Date.now
	},
	userId: {
		type: String,
		required: true
	},
	status: {
		type: String,
		default: 'Done'
	}
})


module.exports = mongoose.model('Transaction', transactionSchema)