const mongoose = require('mongoose');

const userSchema = new mongoose.Schema ({
	firstName: {
		type: String,
		// required: true
	},
	lastName: {
		type: String,
		// required: true
	},
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	loginType: {
		type: String,
		required: [true, 'Login type is required']
	},
});

module.exports = mongoose.model('User', userSchema)