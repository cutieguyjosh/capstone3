const express = require('express')
const router = express.Router()
const auth = require('../auth')
const TransactionController = require('../controllers/Transaction')



// Create a route to add a transaction

router.post('/', auth.verify, async (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		body: req.body,
	};
	res.send(await TransactionController.addTransaction(params));
});



// View all transactions

router.get('/records', auth.verify, async (req, res) => {
	res.send(await TransactionController.records());
})



// Filter income and expense transactions

router.get('/filter-income', auth.verify, async(req, res) => {
	res.send(await TransactionController.recordIncome());
})


router.get('/filter-expense', auth.verify, async(req, res) => {
	res.send(await TransactionController.recordExpense());
})




















// // Update a transaction

// router.put('/:transactionId', auth.verify, (req, res) => {
//     TransactionController.updateTransaction(req).then(result => res.send(result))
// })



// // Delete a transaction

// router.delete('/:transactionId', auth.verify, (req, res) => {
// 	const transactionId = req.params.transactionId
// 	TransactionController.archive({ transactionId }).then(result => res.send(result))
// })





module.exports = router



// const express = require('express');
// const router = express.Router();
// const auth = require('../auth');
// const {
// 	getTransactions,
// 	addTransaction,
// 	updateTransaction,
// 	deleteTransaction
// } = require('../controllers/Transaction');


// router
// 	.route('/')
// 	.get(auth, getTransactions)
// 	.post(auth, addTransaction);


// router
// 	.route('/:id')
// 	.put(auth, updateTransaction)
// 	.delete(auth, deleteTransaction);



// module.exports = router;