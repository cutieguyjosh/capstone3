const express = require('express');
const UserController = require('../controllers/User');
const router = express.Router();
const auth = require('../auth');


// Check for duplicate email

router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(result => res.send(result))
});


// Register a user

router.post('/', (req,res) => {
	UserController.register(req.body).then(result => res.send(result))
});


// Login a user

router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result))
})


// Getting all the details of the user

router.get('/details', auth.verify, (req,res) => {
	// console.log(req.headers.authorization)
	const user = auth.decode(req.headers.authorization)

	UserController.getDetails({ userId: user.id }).then(result => res. send(result))
});


// Verify Google ID Token

router.post('/verify-google-id-token', async (req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
});



module.exports = router